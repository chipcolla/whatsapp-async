from asyncio import get_event_loop, CancelledError
from concurrent.futures.thread import ThreadPoolExecutor
from functools import partial

from driver import WhatsAPIDriver


class WhatsAPIDriverAsync:

    def __init__(self, username="API", proxy=None, command_executor=None, loadstyles=False,
                 profile=None, headless=False, extra_params=None, server=None, port=None, loop=None):

        self.loop = loop or get_event_loop()
        self._pool_executor = ThreadPoolExecutor(max_workers=1)
        self._driver = WhatsAPIDriver(profile, server, port)

    async def _run_async(self, method, *args, **kwargs):
        fut = None
        try:
            fut = self.loop.run_in_executor(self._pool_executor, partial(method, *args, **kwargs))
            return await fut
        except CancelledError:
            if fut:
                fut.cancel()
            raise

    async def connect(self):
        return await self._run_async(self._driver.connect)

    async def get_qr(self):
        return await self._run_async(self._driver.get_qr_code)

    async def get_contacts(self):
        return await self._run_async(self._driver.get_contacts)

    async def test(self):
        self.loop.run_in_executor(self._pool_executor, partial(self._driver.get_qr_code, [], {}))

