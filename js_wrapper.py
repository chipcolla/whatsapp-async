import os

import time

from selenium.common.exceptions import JavascriptException, WebDriverException
from six import string_types


class JsException(Exception):
    def __init__(self, message=None):
        super(Exception, self).__init__(message)


class WAPhoneNotConnectedException(Exception):
    def __init__(self, message=None):
        super(Exception, self).__init__(message)


class JsWrapper:
    def __init__(self, driver):
        self.driver = driver
        self.available_functions = None

    def __getattr__(self, item):
        wa_js_functions = dir(self)

        if item not in wa_js_functions:
            raise AttributeError(f"Function {item} doesn't exist.")

        return JsFunction(item, self.driver, self)

    def __dir__(self):
        if self.available_functions is not None:
            return self.available_functions

        time.sleep(5)
        try:
            script_path = os.path.dirname(os.path.abspath(__file__))
        except NameError:
            script_path = os.getcwd()
        with open(os.path.join(script_path, "static/js", "wa.js"), "r") as script:
            self.driver.execute_script(script.read())

        result = self.driver.execute_script("return window.WAPI")
        if result:
            self.available_functions = result.keys()
            return self.available_functions
        else:
            return []


class JsFunction(object):
    def __init__(self, function_name, driver, wapi_wrapper):
        self.driver = driver
        self.function_name = function_name
        self.wa_wrapper = wapi_wrapper
        self.is_a_retry = False

    def __call__(self, *args, **kwargs):
        # Selenium's execute_async_script passes a callback function that should be called when the JS operation is done
        # It is passed to the WAPI function using arguments[0]
        if len(args):
            command = f'return WAPI.{self.function_name}({",".join([str(JsArg(arg)) for arg in args])}, arguments[0])'

        else:
            command = f'return WAPI.{self.function_name}(arguments[0])'

        try:
            return self.driver.execute_async_script(command)
        except JavascriptException as e:
            if 'WAPI is not defined' in e.msg and self.is_a_retry is not True:
                self.wa_wrapper.available_functions = None
                retry_command = getattr(self.wa_wrapper, self.function_name)
                retry_command.is_a_retry = True
                retry_command(*args, **kwargs)
            else:
                raise JsException('Error in function {self.function_name} ({e.msg}). Command: {command}')
        except WebDriverException as e:
            if e.msg == 'Timed out':
                raise WAPhoneNotConnectedException('Phone not connected to Internet')
            raise JsException('Error in function {self.function_name} ({e.msg}). Command: {command}')


class JsArg(object):
    def __init__(self, obj):
        self.obj = obj

    def __str__(self):
        if isinstance(self.obj, string_types):
            return repr(str(self.obj))

        if isinstance(self.obj, bool):
            return str(self.obj).lower()

        return str(self.obj)

