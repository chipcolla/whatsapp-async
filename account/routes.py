import collections

from account.api.auth import GetToken, VerifyToken
from account.api.contact import ContactAPI
from account.api.qr import QrAPI

dispatcher = collections.namedtuple('Dispatcher', ['method', 'path', 'handler', 'name'])


account_routes = [
    # Token
    dispatcher('POST', '/account/token/auth', GetToken, 'get_token'),
    dispatcher('POST', '/account/token/verify', VerifyToken, 'verify_token'),
    # API
    dispatcher('POST', '/account/api/get-contacts', ContactAPI, 'api_get_contact'),
    dispatcher('POST', '/account/api/get-qr', QrAPI, 'api_get_qr'),
]
