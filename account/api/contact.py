from asyncio import get_event_loop

from aiohttp import web

from account.decorators import check_jwt, check_body_exist

loop = get_event_loop()


class ContactAPI(web.View):

    @check_jwt
    # @check_body_exist
    async def post(self):
        if hasattr(self.request, 'User'):
            user = getattr(self.request, 'User')
            driver_list = self.request.app['driver_list'][user.get('user_email')]
            contacts = await driver_list['driver']._run_async(driver_list['driver'].get_contacts)
            return contacts

        return web.Response(text='Contact!')
