from asyncio import get_event_loop

from aiohttp import web
from aiohttp.web_response import json_response

from account.decorators import check_jwt

loop = get_event_loop()


class QrAPI(web.View):

    @check_jwt
    # @check_body_exist
    async def post(self):
        if self.request.User:
            user = getattr(self.request, 'User')
            driver_list = self.request.app['driver_list'][user.get('user_email')]
            # qr = await driver_list['driver']._run_async(driver_list['driver'].get_qr)
            qr = await driver_list['driver'].get_qr()
            return json_response({'qr': qr})

        return web.Response(text='Contact!')
