import uuid

import asyncio

import datetime

import jwt
from aiohttp import web

from aiohttp.web_response import json_response
from passlib.handlers.pbkdf2 import pbkdf2_sha256

from account.decorators import check_jwt, check_body_exist
from async_driver import WhatsAPIDriverAsync
from db.db import fetch
from settings import JWT_EXP_DELTA_MINUTES, JWT_SECRET, JWT_ALGORITHM

loop = asyncio.get_event_loop()


custom_pbkdf2 = pbkdf2_sha256.using(rounds=120000)

a = uuid.uuid4()


class GetToken(web.View):

    @check_body_exist
    async def post(self):
        # post_data = await self.request.post()
        # pp = await self.request.read()
        post_data = await self.request.json()

        try:
            user = await fetch(self.request.app['pool'],
                               f"select a_a.id, a_a.user_email, a_a.is_superuser, a_a.is_active, a_s.host, a_a.port"
                               f" from account_account as a_a inner join account_server as a_s"
                               f" on a_a.server_id = a_s.id where a_a.email='{post_data.get('email')}'")
            user_password = user[0].get('password')
            if user and custom_pbkdf2.verify(post_data.get('password'), user_password):
                user = user.pop()
                payload = {
                    'user_id': user.get('id'),
                    'user_email': user.get('email'),
                    'is_superuser': user.get('is_superuser'),
                    'is_active': user.get('is_active'),
                    'server': user.get('server'),
                    'port': user.get('port'),
                    'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=JWT_EXP_DELTA_MINUTES)
                }

                uuid_secret_key = uuid.uuid4().hex

                jwt_token = jwt.encode(payload, uuid_secret_key, JWT_ALGORITHM)
                # self.request.app['secret_key'][jwt_token] = uuid_secret_key

                for key, value in self.request.app['secret_key'].items():
                    if value[0] == user.get('id'):
                        del self.request.app['secret_key'][key]
                        break

                self.request.app['secret_key'][jwt_token.decode("utf-8")] = (user.get('id'), uuid_secret_key,)

                if user.get('email') not in self.request.app['driver_list']:
                    driver_instance = WhatsAPIDriverAsync(loop=loop, profile=user.get('email'),
                                                          server=user.get('server'), port=user.get('port'))
                    # driver = await driver_instance.connect()
                    self.request.app['driver_list'][user.get('email')] = {
                        'user_email': user.get('email'),
                        'driver': driver_instance,
                        'created': datetime.datetime.utcnow()
                    }

                return json_response({'token': jwt_token.decode('utf-8')})
        except:
            pass
        return web.Response(text='Auth failed!')


class VerifyToken(web.View):

    # @check_jwt
    @check_body_exist
    async def post(self):

        post_data = await self.request.json()
        jwt_token = post_data.get('token')
        secret_key = self.request.app['secret_key'][jwt_token][1]
        try:
            payload = jwt.decode(jwt_token, secret_key, algorithms=[JWT_ALGORITHM])
        except (jwt.DecodeError, jwt.ExpiredSignatureError):
            return json_response({'token': 'Token verify failed'})

        return json_response({'token': jwt_token})

