import jwt
from aiohttp.web_response import json_response

from settings import JWT_SECRET, JWT_ALGORITHM


async def auth_middleware(app, handler):
    async def middleware(request):
        jwt_token = request.headers.get('Authorization', None)
        if jwt_token:
            try:
                secret_key = request.app['secret_key'][jwt_token][1]
                payload = jwt.decode(jwt_token, secret_key, algorithms=[JWT_ALGORITHM])
            except (jwt.DecodeError, jwt.ExpiredSignatureError):
                request.jwt_auth = request.User = None
            else:
                request.User = {
                    'user_id': payload.get('user_id'),
                    'user_email': payload.get('user_email'),
                    'is_superuser': payload.get('is_superuser'),
                    'is_active': payload.get('is_active'),
                    'server': payload.get('server'),
                    'port': payload.get('port'),
                }
                request.jwt_auth = True
                # return json_response({'message': 'Token is invalid'}, status=400)
        return await handler(request)
    return middleware
