import functools

from aiohttp.web_response import json_response


def check_jwt(func):
    @functools.wraps(func)
    async def wrapper(*args, **kwargs):
        try:
            # f = await func(*args, **kwargs)
            if not args[0].request.jwt_auth:
                return json_response({'message': 'Token is invalid'}, status=400)
            return await func(*args, **kwargs)
        finally:
            pass
    return wrapper


def check_body_exist(func):
    @functools.wraps(func)
    async def wrapper(*args, **kwargs):
        try:
            if not args[0].request.body_exists:
                return json_response({'message': 'Empty body'}, status=400)
            return await func(*args, **kwargs)
        finally:
            pass
    return wrapper