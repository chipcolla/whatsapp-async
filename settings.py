
JWT_SECRET = ''
JWT_ALGORITHM = 'HS256'
JWT_EXP_DELTA_MINUTES = 60 * 60


DB_SETTINGS = {
    'host': '127.0.0.1',
    'user': '',
    'password': '',
    'database': '',
    'port': 5432,
    'min_size': 0,
    'max_size': 3
}