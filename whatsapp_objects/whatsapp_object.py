from weakref import ref


def driver_needed(func):
    def wrapped(self, *args):
        if not self.driver:
            raise AttributeError("No driver passed to object")

        return func(self, *args)

    return wrapped


class WhatsappObject(object):
    def __init__(self, js_obj, driver=None):
        self._js_obj = js_obj
        self._driver = ref(driver)

    @property
    def driver(self):
        return self._driver()

    def get_js_obj(self):
        return self._js_obj


class WhatsappObjectWithId(WhatsappObject):
    def __init__(self, js_obj, driver=None):
        super(WhatsappObjectWithId, self).__init__(js_obj, driver)
        if 'id' in js_obj:
            try:
                self.id = js_obj["id"]["_serialized"]
            except:
                self.id = js_obj["id"]
        if 'name' in js_obj:
            self.name = js_obj["name"]

    def __hash__(self):
        return hash(self.id)

    def __eq__(self, other):
        return self.id == other.id
