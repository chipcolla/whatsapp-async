from multiprocessing.pool import Pool

from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from user_agent import generate_user_agent

from js_wrapper import JsWrapper
from whatsapp_objects.contact import Contact


class WhatsAPIDriver:
    def __init__(self, profile_name, server, port):
        self.options = Options()

        self.options.add_argument("{}".format(generate_user_agent(navigator='chrome')))
        self.options.add_argument("--disable-dev-shm-usage")
        self.options.add_argument("--disable-gpu")
        self.options.add_argument("--disable-extensions")

        self.options.add_argument("--no-sandbox")
        self.options.add_argument("--disable-setuid-sandbox")
        self.options.add_argument("--test-type")

        self.options.add_argument("--user-data-dir=/home/seluser/profiles")

        self.options.add_argument("--profile-directory={}".format(profile_name))
        self.options.add_argument("--whitelisted-ips")

        # self.options.add_argument("start-maximized")
        self.options.add_argument("--disable-popup-blocking")
        self.options.add_argument("disable-infobars")

        self.capabilities = self.options.to_capabilities()

        self.driver = webdriver.Remote(command_executor='XXX:{}/wd/hub'.format(self.port),
                                       desired_capabilities=self.capabilities)
        self.name = profile_name

        self.wapi_functions = JsWrapper(self.driver)

    def connect(self):
        self.driver = webdriver.Remote(command_executor='XXX:{}/wd/hub'.format(4444),
                                       desired_capabilities=self.capabilities)
        return self.driver

    def get_qr_code(self):

        self.driver.get("https://web.whatsapp.com/")
        self.driver.set_page_load_timeout(30)
        self.driver.implicitly_wait(20)

        qr = ''

        try:
            qr = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, "//img[@alt='Scan me!']")))
            qr = qr.get_attribute('src')
        except TimeoutException:
            pass

        # self.driver_quit()

        return qr

    def get_contacts(self):
        all_contacts = self.wapi_functions.getAllContacts()
        return [Contact(contact, self) for contact in all_contacts]

    def driver_quit(self):
        if self.driver:
            self.driver.quit()

    def test(self):
        print('oh num num!')
