import collections

from account.api.auth import GetToken, VerifyToken
from account.api.contact import ContactAPI
from account.routes import account_routes

dispatcher = collections.namedtuple('Dispatcher', ['method', 'path', 'handler', 'name'])


routes = account_routes
