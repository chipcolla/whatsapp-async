from asyncio import get_event_loop

import aiohttp_jinja2
import asyncpg
import jinja2
import os

from aiohttp import web

from account.middleware.account import auth_middleware
from async_driver import WhatsAPIDriverAsync
from routes import routes
from settings import DB_SETTINGS


async def init_app():
    app = web.Application(middlewares=[auth_middleware])
    aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader('templates'))
    app['driver_list'] = dict()
    app['secret_key'] = dict()
    # app['driver'] = WhatsAPIDriverAsync(loop=loop, profile='Profile 2')
    app['pool'] = await asyncpg.create_pool(**DB_SETTINGS)
    for route in routes:
        app.router.add_route(
            method=route.method,
            path=route.path,
            handler=route.handler,
            name=route.name
        )
    return app


loop = get_event_loop()
app = loop.run_until_complete(init_app())
web.run_app(app, host='localhost')
