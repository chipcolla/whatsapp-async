import asyncpg


async def fetch(pool, query, *args):
    async with pool.acquire() as connection:
        async with connection.transaction():
            result = await connection.fetch(query)
            return [dict(row) for row in result]
